# Makefile for a latex paper
LATEX = TEXINPUTS=$(TEXINPUTS):packages pdflatex
PAPER = mills

all: $(PAPER).pdf

# Run once, then re-run until it's happy
# Input redirected from /dev/null is like hitting ^C at first error
$(PAPER).pdf: $(wildcard text/*tex) $(PAPER).tex ./bibliography.bib
	$(LATEX) $(PAPER).tex </dev/null
	bibtex $(PAPER)
	$(LATEX) $(PAPER).tex </dev/null
	$(LATEX) $(PAPER).tex </dev/null

Figures/%.pdf: $(wildcard graphs/*pdf)
	pdfcrop $< $@

clean:
	rm -f *.aux *.log $(PAPER).ps $(PAPER).pdf *.dvi *.blg *.bbl *.lof *.lot *.toc *~ *.out
